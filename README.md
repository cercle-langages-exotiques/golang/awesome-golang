# Awesome Golang

😎 Awesome lists about all kinds of interesting topics for Golang

# Summary 

## Ecosystem

- Front-End Development
- Back-End Development
- [Databases](#databases)
- [Architecture](#architecture)
- Networking
- Decentralized Systems
- Events
- [Testing](#testing)
- Security
- [Algorithms](#algorithms)
- Content Management Systems
- Hardware
- Big Data
- Development Environment
- [Best Practice](#bestpractice)

## Learning Ressources

- Theory
- [Books](#books)
- Media
- [Blog](#blog)


# Ecosystem

## Databases

* [**Redix**](https://github.com/alash3al/redix)
A very simple pure key => value storage system that speaks Redis protocol with Postgres as storage engine and more

## Architecture

* [**Go Clean Archi**](https://github.com/bxcodec/go-clean-arch) Go (Golang) Clean Architecture based on Reading Uncle Bob's Clean Architecture
* [**Go Clean Archi with Gin Framework**](https://golangexample.com/clean-architecture-using-golang-with-gin-framework/) Template Structure
* [**Wire**](https://github.com/google/wire) Injection de dépendances
* [**Go Benchmark Performances**](https://benchmarksgame-team.pages.debian.net/benchmarksgame/measurements/go.html) All Go programs & measurements

## Testing

* [**Gnomock**](https://github.com/orlangure/gnomock) tests without mocks

## Algorithms

* [**Goraph**](https://github.com/gyuho/goraph)
 Package goraph implements graph data structure and algorithms.

# Learning Resources

## Books

* [**100 Go Mistakes**](https://github.com/teivah/100-go-mistakes) Source code of 100 Go Mistakes 📖

## Blog

* [**Rust vs. Go: Why They’re Better Together**](https://thenewstack.io/rust-vs-go-why-theyre-better-together/)
* [**Rust vs. Go**](https://bitfieldconsulting.com/golang/rust-vs-go) How do the two compare in areas like performance, simplicity, safety, features, scale, and concurrency?...
* [**Awesome Go Educations**](https://mehdihadeli.github.io/awesome-go-education/clean-architecture/) A curated list of awesome articles and resources for learning and practicing Golang and its related technologies. 
* [**Awesome Go**](https://awesome-go.com/) A curated list of awesome Go frameworks, libraries and software.

##  BestPractice

* [**Generics can make your go code slower**](https://planetscale.com/blog/generics-can-make-your-go-code-slower)
* [**Uber Go Style Guide**](https://github.com/uber-go/guide/blob/master/style.md)
